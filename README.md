# fonts.txt

**IMPORTANT**: This repository is deprecated. See the [typebits](https://gitlab.com/typebits/) organisation repositories for current versions of these scripts.

A collection of scripts and fonts made in and for [Manufactura
Independente](http://manufacturaindependente.org)'s bitmap type design
workshops.

TODO: Document each script, font and repo structure

