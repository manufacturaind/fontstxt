#!/usr/bin/env python
import jinja2
import click
import os
import shutil
import json

scriptdir = os.path.dirname(__file__)
TEMPLATE_PATH = os.path.join(scriptdir, 'skeleton/json-template-single')
PAYLOAD_PATH = os.path.join(scriptdir, 'skeleton/options-single')


@click.command()
@click.option("-f", "--font-name", prompt='Font/Family name')
@click.option("-n", "--designer-name", prompt='Designer name')
@click.option("-u", "--designer-url", prompt='Designer URL', default='')
@click.option("-m", "--manufacturer-name", prompt='Manufacturer name', default='')
@click.option("-v", "--vendor-url", prompt='Vendor URL', default='')
@click.option("-d", "--description", prompt='Description (1 paragraph max)', default='')
@click.option("-t", "--trademark", prompt='Trademark notice', default='')
@click.option("-g", "--glyphs-dir")
@click.option("-h", "--height", default=10)
@click.option("-w", "--width", default=6)
def generate_font(font_name, designer_name, designer_url, manufacturer_name, vendor_url, description, trademark, glyphs_dir, height, width):
    context = {"font_name": font_name,
               "file_name": font_name.replace(' ', ''),
               "designer_name": designer_name,
               "designer_url": designer_url,
               "manufacturer_name": manufacturer_name,
               "vendor_url": vendor_url,
               "description": description + " Built with graphicoreBMFB and Fontforge.",
               "trademark_notice": trademark,
               }

    output_dir = context["file_name"]
    if os.path.exists(output_dir):
        # shutil.rmtree(output_dir)
        click.echo("Directory %s exists, not going on.")
        import sys
        sys.exit()
    os.mkdir(output_dir)

    loader = jinja2.FileSystemLoader(searchpath=TEMPLATE_PATH)
    env = jinja2.Environment(loader=loader)

    for t in os.listdir(TEMPLATE_PATH):
        template = env.get_template(t)
        output = template.render(context)
        filename = os.path.join(output_dir, t.replace('BitmapFont', context['file_name'] + '-'))
        f = open(filename, 'w')
        f.write(output)
        f.close()

    shutil.copytree(PAYLOAD_PATH, os.path.join(output_dir, 'options/'))

    if glyphs_dir:
        shutil.copytree(glyphs_dir, os.path.join(output_dir, 'glyphs/'))
    else:
        # generate empty glyphs
        os.makedirs(os.path.join(output_dir, 'glyphs/'))
        empty_glyph = (('.' * width + '\n') * height).strip()
        glyph_json = json.loads(open(os.path.join(output_dir, 'options/glyphs.jsn'), 'r').read())
        glyph_list = list(set(glyph_json['glyphs'].values()))
        for glyph_filename in glyph_list:
            f = open(os.path.join(output_dir, 'glyphs/', glyph_filename), 'w')
            f.write(empty_glyph)
            f.close()

    click.echo("Done! The license was automatically set to the Open Font License (OFL).")

if __name__ == "__main__":
    generate_font()
