from PIL import Image
import os

from settings import *

if not os.path.exists(OUT_DIR):
    os.mkdir(OUT_DIR)

im = Image.open(TEMPIMAGEFILE)
width, height = im.size
im = im.crop((1, 1, width - 1, height - 1))

source_lines = open(CHARACTERSFILE, 'r').readlines()
grid_width = max([len(line) for line in source_lines]) - 1
grid_height = len(source_lines)

font_width = (width - grid_width - 1) / grid_width
font_height = (height - grid_height - 1) / grid_height

letter_matrix = []
for line in source_lines:
    line = line.strip()
    charlist = [char for char in line]
    letter_matrix.append(charlist)

linecount = 0
for line in letter_matrix:
    charcount = 0
    for char in line:
        if char.isupper():
            filename = "%sCap.txt" % char.lower()
        else:
            filename = "%s.txt" % char

        outfile = os.path.join(OUT_DIR, filename)
        f = open(outfile, 'w')

        # determine location of the char in the image
        point1 = ((font_width + 1) * charcount,
                  (font_height + 1) * linecount)
        point2 = ((font_width + 1) * charcount + font_width,
                  (font_height + 1) * linecount + font_height)
        for y in range(point1[1], point2[1]):
            for x in range(point1[0], point2[0]):
                color = im.getpixel((x, y))
                if color == (255, 255, 255):
                    f.write(".")
                elif color == (0, 0, 0):
                    f.write("#")
                else:
                    f.write(".")
            f.write("\n")
        f.close()
        charcount += 1
    linecount += 1

# create space character
outfile = os.path.join(OUT_DIR, '_space.txt')
f = open(outfile, 'w')
point1 = (0, 0)
point2 = (font_width, font_height)
for y in range(point1[1], point2[1]):
    for x in range(point1[0], point2[0]):
        f.write(".")
    f.write("\n")
f.close()

# run graphicore font generator
font_name="Retrofest"
designer_name="Adam Sofokleous, Camellia Xueyi, David Moulton, Jennifer Steele, Jens Aronsson, Johan Hermansson, Louis Reed, Metod Blejec, Oliver Roick"
designer_url=""
manufacturer_name="Manufactura Independente"
vendor_url="http://manufacturaindependente.org"
description="Collaboratively created at Mozilla Festival 2014."
trademark=""
glyphs_dir=OUT_DIR
height=font_height # Unused

cmd = " ".join(("python graphicoreBMFB/scripts/create_new_font.py ",
"--font-name='%s'" % font_name,
"--designer-name='%s'" % designer_name,
"--designer-url='%s'" % designer_url,
"--manufacturer-name='%s'" % designer_url,
"--vendor-url='%s'" % vendor_url,
"--description='%s'" % description,
"--trademark='%s'" % trademark,
"--glyphs-dir='%s'" % glyphs_dir,
"--height='%s'" % height
))
      
os.system(cmd)






