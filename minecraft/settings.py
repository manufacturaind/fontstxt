LEVELFILE = "/home/ana/blocktype/Typeface/level.dat"

BASEPOINT = (-244, 4, 346)

IMAGEFILE = "testspecimen.png"

TEMPIMAGEFILE = "snapshot.png"
CHARACTERSFILE = 'characters.txt'
OUT_DIR = "./output/"

BLOCKS = {
        (255, 255, 255): "95", # White Stained Glass
        (154, 0, 0):     "95:14", # Red stained glass
        (0, 0, 0): "95:15", # Black stained glass
        }

WHITE_BLOCK = (95, 0)
BLACK_BLOCK = (95, 15)
BORDER_BLOCK = (95, 14)
MARKER_BLOCK = (48, 0)

