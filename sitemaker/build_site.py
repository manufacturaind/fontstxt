import jinja2
import glob
import os

output_dir = "html"

def get_fonts():
    fonts = []
    fontfiles = glob.glob("./fonts/*.otf")
    for font in fontfiles:
        fontname = font.split('/')[-1].replace('.otf', '')
        fonts.append(fontname)
    return fonts

env = jinja2.Environment(loader=jinja2.FileSystemLoader(["templates"]))
template = env.get_template("index.html")

context = {"fonts": get_fonts()}

contents = template.render(**context)
import codecs
f = codecs.open(os.path.join(output_dir, "index.html"), 'w', 'utf-8')
f.write(contents)
f.close()
